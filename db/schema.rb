# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150618141153) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: true do |t|
    t.string   "title"
    t.text     "header"
    t.text     "body"
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "approved"
    t.integer  "author_id"
  end

  add_index "articles", ["author_id"], name: "index_articles_on_author_id", using: :btree

  create_table "children_circles", force: true do |t|
    t.string   "name"
    t.string   "boss"
    t.text     "description"
    t.string   "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "cinemas", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "cirques", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.text     "description"
    t.string   "boss"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "comments", force: true do |t|
    t.text     "text"
    t.datetime "date"
    t.integer  "user_id"
    t.integer  "votes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "contacts", force: true do |t|
    t.string   "type"
    t.string   "contact"
    t.integer  "contactable_id"
    t.string   "contactable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contact_type"
  end

  create_table "controller_offices", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "address"
    t.string   "boss"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "events", force: true do |t|
    t.date     "date"
    t.string   "name"
    t.string   "description"
    t.string   "category"
    t.string   "status"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "location"
  end

  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "health_facilities", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "hospitals", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "images", force: true do |t|
    t.string   "alt",               default: ""
    t.string   "hint",              default: ""
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kindergartens", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "libraries", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "maternity_hospitals", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "notifications", force: true do |t|
    t.integer  "user_id"
    t.text     "text"
    t.string   "header"
    t.boolean  "read",        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
    t.integer  "ref_user_id"
    t.string   "link_title"
  end

  add_index "notifications", ["ref_user_id"], name: "index_notifications_on_ref_user_id", using: :btree

  create_table "pages", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "police", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "address"
    t.string   "boss"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "policlinics", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "schools", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
    t.string   "img_file_name"
  end

  create_table "social_objects", force: true do |t|
    t.string   "name"
    t.string   "type"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "state_institutions", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "stomatologies", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "boss"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lat"
    t.float    "lng"
  end

  create_table "tests", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: ""
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "nickname"
    t.boolean  "admin"
  end

  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
