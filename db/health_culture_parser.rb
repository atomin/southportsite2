require 'csv'
require_relative 'geolocation'

def parse_health(klass, file_path, *types)

  klass.destroy_all
  CSV.parse( File.open(file_path){ |f| f.read}) do |r|

    type = r[0]
    name = r[1]
    address = r[2]
    boss = (r[3] && r[3].gsub(/\(.*/, '').strip) || ''
    sites = r[4] && r[4].split(',') || []
    description = r[5]
    phones = r[6] && r[6].split(',') || []

      #inside_contact: r[9] 
    h = {
      name: name,
      address: address,
      description: description
    }

    h[:boss] = boss if klass.column_names.include? 'boss'

    next if !types.empty? && !types.include?(type)

    location = get_location(address)

    h[:lat] = location[:lat]
    h[:lng] = location[:lng]


    s = klass.new(h)
    p 'sites:'
    sites.each do |site|
      s.contacts << Contact.new(contact_type: 'site', contact: site.strip)
      p site
    end
    p 'phones:'
    phones.each do |phone|
      s.contacts << Contact.new(contact_type: 'phone', contact: phone.strip)
      p phone
    end
    s.save
  end
end

def parse_hospital()
  p '------'
  p 'parse_hospital'
  parse_health(Hospital, 'db/health.csv', 'Больница')
end

def parse_stomatology()
  p '------'
  p 'parse_stomatology'
  parse_health(Stomatology, 'db/health.csv', 'Стоматологическая поликлиника')
end

def parse_maternity_hospital()
  p '------'
  p 'parse_maternity_hospital'
  parse_health(MaternityHospital, 'db/health.csv', 'Роддом')
end

def parse_policlinic()
  p '------'
  p 'parse_policlinic'
  parse_health(Policlinic, 'db/health.csv', 'Поликлиника', 'КВД', 'ПНД')
end

def parse_library()
  p '------'
  p 'parse_library'
  parse_health(Library, 'db/culture.csv', 'Библиотека')
end

def parse_cirque()
  p '------'
  p 'parse_library'
  parse_health(Cirque, 'db/culture.csv', 'Цирк')
end

def parse_cinema()
  p '------'
  p 'parse_cinema'
  parse_health(Cinema, 'db/culture.csv', 'Кинотеатр')
end

def parse_police()
  p '------'
  p 'parse_police'
  parse_health(Police, 'db/state_institution.csv', 'Полиция')
end

def parse_controller_office()
  p '------'
  p 'parse_controller_office'
  parse_health(ControllerOffice, 'db/state_institution.csv', 'Диспетчерская')
end

def parse_state_institution()
  p '------'
  p 'parse_state_institution'
  parse_health(StateInstitution, 'db/state_institution.csv', 
    'Собес',
    'МФЦ',
    'ЕИРЦ',
    'ДЕЗ',
    'ИС',
    'Управа')
end

