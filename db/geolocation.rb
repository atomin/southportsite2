# require 'net/http'

def get_location(address)
  sleep 1

  p "address: #{address}"
  url = URI.parse('http://maps.google.com/maps/api/geocode/json?address=' + URI.encode(address))
  req = Net::HTTP::Get.new(url.to_s)
  res = Net::HTTP.start(url.host, url.port) {|http| http.request(req)  }
  r = JSON.parse(res.body)
  p r
  r["results"][0]["geometry"]["location"].symbolize_keys

end