require_relative 'education_parser'
require_relative 'health_culture_parser'

Page.find_or_create_by name: 'work'

test_user = User.new(
  email: 'test@test.ru', 
  nickname: 'test', 
  password: 'password',
  password_confirmation: 'password',
  )

if !test_user.save
  test_user = User.find_by email: 'test@test.ru'
end

articles = YAML.load_file("#{Rails.root}/db/articles.yml")['articles']
articles.each do |a|
  Article.find_or_create_by(a)
end

events = YAML.load_file("#{Rails.root}/db/events.yml")['events']
events.each do |e|
  e = Event.find_or_create_by(e)
  e.user = test_user
  e.date = Date.today + 3
end

parse_schools
parse_kindergartens
parse_circles


parse_hospital
parse_stomatology
parse_maternity_hospital
parse_policlinic

parse_library
parse_cirque
parse_cinema

parse_police
parse_controller_office
parse_state_institution