class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :header
      t.text :body
      t.datetime :date

      t.timestamps
    end
  end
end
