class ChangeCommentsDateColumn < ActiveRecord::Migration
  def change
    change_table :comments do |t|
      t.change :date, :datetime
    end
  end
end
