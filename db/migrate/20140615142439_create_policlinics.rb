class CreatePoliclinics < ActiveRecord::Migration
  def change
    create_table :policlinics do |t|
      t.string :name
      t.string :address
      t.string :boss
      t.text :description

      t.timestamps
    end
  end
end
