class AddCoordinatesToInstitution < ActiveRecord::Migration
  def change
    for t in [:schools, :stomatologies, :state_institutions, :policlinics, :police, :maternity_hospitals, :libraries, :kindergartens, :hospitals, :health_facilities, :cirques, :cinemas, :children_circles]
      add_column t, :lat, :float
      add_column t, :lng, :float
    end
  end
end
