class CreatePolice < ActiveRecord::Migration
  def change
    create_table :police do |t|
      t.string :name
      t.text :description
      t.string :address
      t.string :boss

      t.timestamps
    end
  end
end
