class RenameEventTypeColumn < ActiveRecord::Migration
  def change
    change_table :events do |t|
      t.rename :type, :category
    end
  end
end
