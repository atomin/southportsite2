class CreateChildrenCircles < ActiveRecord::Migration
  def change
    create_table :children_circles do |t|
      t.string :name
      t.string :boss
      t.text :description
      t.string :address

      t.timestamps
    end
  end
end
