class ApplySchoolsFileNames < ActiveRecord::Migration
  def change
    s = School.find_by(name: "Гимназия №1274 им Маяковского (бывшая школа №79, №483)")
    s.update(img_file_name: 'Гимназия№1274.jpg')
    s = School.find_by(name: "Второе здание (младшие классы) гимназии №1274 (бывшая школа №469)")
    s.update(img_file_name: 'Гимназия№1274_школа№469.jpg')
    s = School.find_by(name: "Второе здание школы №513 (бывшая школа №462)")
    s.update(img_file_name: 'Школа№513_бывшая462.jpg')
  end
end
