class CreateStomatologies < ActiveRecord::Migration
  def change
    create_table :stomatologies do |t|
      t.string :name
      t.string :address
      t.string :boss
      t.text :description

      t.timestamps
    end
  end
end
