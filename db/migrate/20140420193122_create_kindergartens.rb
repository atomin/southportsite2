class CreateKindergartens < ActiveRecord::Migration
  def change
    create_table :kindergartens do |t|
      t.string :name
      t.string :address
      t.string :boss
      t.text :description

      t.timestamps
    end
  end
end
