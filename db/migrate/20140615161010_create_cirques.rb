class CreateCirques < ActiveRecord::Migration
  def change
    create_table :cirques do |t|
      t.string :name
      t.string :address
      t.text :description
      t.string :boss

      t.timestamps
    end
  end
end
