class CreateSchoolContacts < ActiveRecord::Migration
  def change
    create_table :school_contacts do |t|
      t.string :type
      t.string :contact
      t.references :school, index: true

      t.timestamps
    end
  end
end
