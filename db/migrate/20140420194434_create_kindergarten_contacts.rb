class CreateKindergartenContacts < ActiveRecord::Migration
  def change
    create_table :kindergarten_contacts do |t|
      t.string :type
      t.string :contact
      t.references :kindergarten, index: true

      t.timestamps
    end
  end
end
