class CreateLibraryContacts < ActiveRecord::Migration
  def change
    create_table :library_contacts do |t|
      t.string :type
      t.string :contact
      t.references :library, index: true

      t.timestamps
    end
  end
end
