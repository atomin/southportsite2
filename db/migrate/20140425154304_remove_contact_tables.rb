class RemoveContactTables < ActiveRecord::Migration
  def change
    drop_table :school_contacts
    drop_table :library_contacts
    drop_table :kindergarten_contacts
    drop_table :health_facility_contacts
  end
end
