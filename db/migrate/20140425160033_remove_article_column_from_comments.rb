class RemoveArticleColumnFromComments < ActiveRecord::Migration
  def change
    change_table :comments do |t|
      t.remove :article_id
    end
  end
end
