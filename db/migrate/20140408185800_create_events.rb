class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :date
      t.string :name
      t.string :description
      t.string :type
      t.string :status
      t.references :user, index: true

      t.timestamps
    end
  end
end
