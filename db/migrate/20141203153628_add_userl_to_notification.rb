class AddUserlToNotification < ActiveRecord::Migration
  def change
    add_reference :notifications, :ref_user, index: true
  end
end
