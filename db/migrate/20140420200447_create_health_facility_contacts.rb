class CreateHealthFacilityContacts < ActiveRecord::Migration
  def change
    create_table :health_facility_contacts do |t|
      t.string :type
      t.string :contant
      t.references :health_facility, index: true

      t.timestamps
    end
  end
end
