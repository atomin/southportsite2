class CreateControllerOffices < ActiveRecord::Migration
  def change
    create_table :controller_offices do |t|
      t.string :name
      t.text :description
      t.string :address
      t.string :boss

      t.timestamps
    end
  end
end
