class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :text
      t.date :date
      t.references :user, index: true
      t.references :article, index: true
      t.integer :votes

      t.timestamps
    end
  end
end
