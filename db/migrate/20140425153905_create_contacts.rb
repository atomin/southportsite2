class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :type
      t.string :contact
      t.integer :contactable_id
      t.string :contactable_type

      t.timestamps
    end
  end
end
