class AddCoordinatesToControllerOffice < ActiveRecord::Migration
  def change
    add_column :controller_offices, :lat, :float
    add_column :controller_offices, :lng, :float
  end
end
