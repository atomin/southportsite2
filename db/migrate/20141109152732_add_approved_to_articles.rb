class AddApprovedToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :approved, :bool
  end
end
