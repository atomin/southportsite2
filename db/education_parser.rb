require 'csv'
require_relative 'geolocation'

def parse_education(klass, file_path, *types)

  klass.destroy_all
  CSV.parse( File.open(file_path){ |f| f.read}) do |r|

    type = r[1]
    name = r[2]
    address = r[3]
    boss = (r[4] && r[4].gsub(/\(.*/, '').strip) || ''
    sites = r[5] && r[5].split(',') || []
    description = r[6]
    comments_sites = r[7] && r[7].split(',') || []
    social_sites = r[8] && r[8].split(',') || []

      #inside_contact: r[9] 
    h = {
      name: name,
      address: address,
      boss: boss,
      description: description
    }
    next unless types.empty? || types.include?(type)

    location = get_location(address)

    h[:lat] = location[:lat]
    h[:lng] = location[:lng]

    s = klass.new(h)
    p 'sites:'
    sites.each do |site|
      s.contacts << Contact.new(contact_type: 'site', contact: site.strip)
      p site
    end
    p 'comments_sites:'
    comments_sites.each do |site|
      s.contacts << Contact.new(contact_type: 'comment_site', contact: site.strip)
      p site
    end
    p 'social_sites:'
    social_sites.each do |site|
      s.contacts << Contact.new(contact_type: 'social_site', contact: site.strip)
      p site
    end
    s.save
  end
end

def parse_schools()
  p '------'
  parse_education(School, 'db/schools.csv', 'СОШ', 'Гимназия')
end

def parse_kindergartens()
  p '------'
  p 'parse_kindergartens'
  parse_education(Kindergarten, 'db/schools.csv', 'ДОУ')
end

def parse_circles()
  p '------'
  p 'parse_circles'
  parse_education(ChildrenCircle, 'db/circles.csv')
end