SouthPortSite2::Application.routes.draw do


  root "home#index"
  get "notifications/index"
  get "about", to: 'about#index'
  get "links", to: 'links#index'
  get "links/test", to: 'links#test'
  get "helpful", to: 'helpful#index'
  get "events", to: 'events#index'

  get '/map', to: 'info#map' 
  get '/infolist/:entityName', to: 'info#index' 
  get '/institution', to: 'info#institution', as: :institution
  get '/info/:entityName/:entityId', to: 'info#institution_page', as: :institution_page
  get "notifications", to: 'notifications#index'

  resources :articles, only: [:show, :index]

  resources :comments, only: [:create, :edit, :update, :destroy]

  resources :events

  get "users_profile/:id", to: 'users_profile#show', as: :user

  devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks" }

  namespace :admin do
    resources :articles
    patch '/articles/:id/set_approved', to: 'articles#set_approved', as: :article_set_approved
  end


  post '/tinymce_assets' => 'tinymce_assets#create'


  get "test/index"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end