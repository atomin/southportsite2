require 'test_helper'

class StateInstitutionsControllerTest < ActionController::TestCase
  setup do
    @state_institution = state_institutions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:state_institutions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create state_institution" do
    assert_difference('StateInstitution.count') do
      post :create, state_institution: { address: @state_institution.address, boss: @state_institution.boss, description: @state_institution.description, name: @state_institution.name }
    end

    assert_redirected_to state_institution_path(assigns(:state_institution))
  end

  test "should show state_institution" do
    get :show, id: @state_institution
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @state_institution
    assert_response :success
  end

  test "should update state_institution" do
    patch :update, id: @state_institution, state_institution: { address: @state_institution.address, boss: @state_institution.boss, description: @state_institution.description, name: @state_institution.name }
    assert_redirected_to state_institution_path(assigns(:state_institution))
  end

  test "should destroy state_institution" do
    assert_difference('StateInstitution.count', -1) do
      delete :destroy, id: @state_institution
    end

    assert_redirected_to state_institutions_path
  end
end
