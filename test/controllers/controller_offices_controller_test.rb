require 'test_helper'

class ControllerOfficesControllerTest < ActionController::TestCase
  setup do
    @controller_office = controller_offices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:controller_offices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create controller_office" do
    assert_difference('ControllerOffice.count') do
      post :create, controller_office: { address: @controller_office.address, boss: @controller_office.boss, description: @controller_office.description, name: @controller_office.name }
    end

    assert_redirected_to controller_office_path(assigns(:controller_office))
  end

  test "should show controller_office" do
    get :show, id: @controller_office
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @controller_office
    assert_response :success
  end

  test "should update controller_office" do
    patch :update, id: @controller_office, controller_office: { address: @controller_office.address, boss: @controller_office.boss, description: @controller_office.description, name: @controller_office.name }
    assert_redirected_to controller_office_path(assigns(:controller_office))
  end

  test "should destroy controller_office" do
    assert_difference('ControllerOffice.count', -1) do
      delete :destroy, id: @controller_office
    end

    assert_redirected_to controller_offices_path
  end
end
