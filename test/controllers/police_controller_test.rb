require 'test_helper'

class PoliceControllerTest < ActionController::TestCase
  setup do
    @police = police(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:police)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create police" do
    assert_difference('Police.count') do
      post :create, police: { address: @police.address, boss: @police.boss, description: @police.description, name: @police.name }
    end

    assert_redirected_to police_path(assigns(:police))
  end

  test "should show police" do
    get :show, id: @police
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @police
    assert_response :success
  end

  test "should update police" do
    patch :update, id: @police, police: { address: @police.address, boss: @police.boss, description: @police.description, name: @police.name }
    assert_redirected_to police_path(assigns(:police))
  end

  test "should destroy police" do
    assert_difference('Police.count', -1) do
      delete :destroy, id: @police
    end

    assert_redirected_to police_index_path
  end
end
