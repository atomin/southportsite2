require 'test_helper'

class StomatologiesControllerTest < ActionController::TestCase
  setup do
    @stomatology = stomatologies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:stomatologies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create stomatology" do
    assert_difference('Stomatology.count') do
      post :create, stomatology: { address: @stomatology.address, boss: @stomatology.boss, description: @stomatology.description, name: @stomatology.name }
    end

    assert_redirected_to stomatology_path(assigns(:stomatology))
  end

  test "should show stomatology" do
    get :show, id: @stomatology
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @stomatology
    assert_response :success
  end

  test "should update stomatology" do
    patch :update, id: @stomatology, stomatology: { address: @stomatology.address, boss: @stomatology.boss, description: @stomatology.description, name: @stomatology.name }
    assert_redirected_to stomatology_path(assigns(:stomatology))
  end

  test "should destroy stomatology" do
    assert_difference('Stomatology.count', -1) do
      delete :destroy, id: @stomatology
    end

    assert_redirected_to stomatologies_path
  end
end
