require 'test_helper'

class CirquesControllerTest < ActionController::TestCase
  setup do
    @cirque = cirques(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cirques)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cirque" do
    assert_difference('Cirque.count') do
      post :create, cirque: { address: @cirque.address, boss: @cirque.boss, description: @cirque.description, name: @cirque.name }
    end

    assert_redirected_to cirque_path(assigns(:cirque))
  end

  test "should show cirque" do
    get :show, id: @cirque
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cirque
    assert_response :success
  end

  test "should update cirque" do
    patch :update, id: @cirque, cirque: { address: @cirque.address, boss: @cirque.boss, description: @cirque.description, name: @cirque.name }
    assert_redirected_to cirque_path(assigns(:cirque))
  end

  test "should destroy cirque" do
    assert_difference('Cirque.count', -1) do
      delete :destroy, id: @cirque
    end

    assert_redirected_to cirques_path
  end
end
