require 'test_helper'

class PoliclinicsControllerTest < ActionController::TestCase
  setup do
    @policlinic = policlinics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:policlinics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create policlinic" do
    assert_difference('Policlinic.count') do
      post :create, policlinic: { address: @policlinic.address, boss: @policlinic.boss, description: @policlinic.description, name: @policlinic.name }
    end

    assert_redirected_to policlinic_path(assigns(:policlinic))
  end

  test "should show policlinic" do
    get :show, id: @policlinic
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @policlinic
    assert_response :success
  end

  test "should update policlinic" do
    patch :update, id: @policlinic, policlinic: { address: @policlinic.address, boss: @policlinic.boss, description: @policlinic.description, name: @policlinic.name }
    assert_redirected_to policlinic_path(assigns(:policlinic))
  end

  test "should destroy policlinic" do
    assert_difference('Policlinic.count', -1) do
      delete :destroy, id: @policlinic
    end

    assert_redirected_to policlinics_path
  end
end
