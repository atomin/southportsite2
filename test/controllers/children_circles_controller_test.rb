require 'test_helper'

class ChildrenCirclesControllerTest < ActionController::TestCase
  setup do
    @children_circle = children_circles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:children_circles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create children_circle" do
    assert_difference('ChildrenCircle.count') do
      post :create, children_circle: { address: @children_circle.address, boss: @children_circle.boss, description: @children_circle.description, name: @children_circle.name }
    end

    assert_redirected_to children_circle_path(assigns(:children_circle))
  end

  test "should show children_circle" do
    get :show, id: @children_circle
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @children_circle
    assert_response :success
  end

  test "should update children_circle" do
    patch :update, id: @children_circle, children_circle: { address: @children_circle.address, boss: @children_circle.boss, description: @children_circle.description, name: @children_circle.name }
    assert_redirected_to children_circle_path(assigns(:children_circle))
  end

  test "should destroy children_circle" do
    assert_difference('ChildrenCircle.count', -1) do
      delete :destroy, id: @children_circle
    end

    assert_redirected_to children_circles_path
  end
end
