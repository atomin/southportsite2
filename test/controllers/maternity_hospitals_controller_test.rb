require 'test_helper'

class MaternityHospitalsControllerTest < ActionController::TestCase
  setup do
    @maternity_hospital = maternity_hospitals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:maternity_hospitals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create maternity_hospital" do
    assert_difference('MaternityHospital.count') do
      post :create, maternity_hospital: { address: @maternity_hospital.address, boss: @maternity_hospital.boss, description: @maternity_hospital.description, name: @maternity_hospital.name }
    end

    assert_redirected_to maternity_hospital_path(assigns(:maternity_hospital))
  end

  test "should show maternity_hospital" do
    get :show, id: @maternity_hospital
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @maternity_hospital
    assert_response :success
  end

  test "should update maternity_hospital" do
    patch :update, id: @maternity_hospital, maternity_hospital: { address: @maternity_hospital.address, boss: @maternity_hospital.boss, description: @maternity_hospital.description, name: @maternity_hospital.name }
    assert_redirected_to maternity_hospital_path(assigns(:maternity_hospital))
  end

  test "should destroy maternity_hospital" do
    assert_difference('MaternityHospital.count', -1) do
      delete :destroy, id: @maternity_hospital
    end

    assert_redirected_to maternity_hospitals_path
  end
end
