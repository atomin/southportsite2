json.array!(@events) do |event|
  json.extract! event, :id, :date, :name, :description, :type, :status, :user_id
  json.url event_url(event, format: :json)
end
