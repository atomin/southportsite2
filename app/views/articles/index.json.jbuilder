json.array!(@articles) do |article|
  json.extract! article, :id, :title, :header, :body, :date
  json.url article_url(article, format: :json)
end
