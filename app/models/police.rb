class Police < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def self.icon
    'MapIcons/police2.png'
  end  

  def self.ru_name
    'Полиция'
  end  

  def self.topic
    'Гос. учреждения'
  end
end
