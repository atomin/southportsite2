class HealthFacility < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def boss_post
    'Главный врач'
  end

  def self.ru_name
    'Здоровье'
  end  

  def self.topic
    'Медицина'
  end
end
