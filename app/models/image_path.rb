module ImagePath

  def img_path
    file_name = self.try(:img_file_name) || "#{self.name.gsub(' ', '')}.jpg"
    path = "Objects/#{file_name}"
    File.exists?("app/assets/images/#{path}") ? path : nil
  end
end
