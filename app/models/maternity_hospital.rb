class MaternityHospital < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def boss_post
    'Главный врач'
  end

  def self.icon
    'MapIcons/nursery.png'
  end  

  def self.ru_name
    'Род дома'
  end  

  def self.topic
    'Медицина'
  end
end
