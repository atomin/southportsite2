class School < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  include ImagePath

  def boss_post
    'Директор'
  end

  def self.icon
    'MapIcons/school2.png'
  end

  def self.ru_name
    'Школы'
  end  

  def self.topic
    'Образование'
  end
end
