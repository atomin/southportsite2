class Hospital < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def boss_post
    'Главный врач'
  end

  def self.icon
    'MapIcons/hospital-building.png'
  end  

  def self.ru_name
    'Больницы'
  end  

  def self.topic
    'Медицина'
  end
end
