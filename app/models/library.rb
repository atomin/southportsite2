class Library < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def self.icon
    'MapIcons/book.png'
  end

  def self.ru_name
    'Библиотеки'
  end  

  def self.topic
    'Культура'
  end
end
