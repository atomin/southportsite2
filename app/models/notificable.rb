module Notificable 
  def add_notification(comment, path, user)
    users = comments.where.not(user: user).map {|c| c.user}
    auth = try(:author)
    users << auth if auth && user != auth
    users.uniq.each do |u|
      notification = Notification.new( 
        header: "оставил новый комментарий к статье", 
        url: path,
        text: comment.html_text,
        ref_user: u,
        link_title: title
      )
      u.notifications << notification
      notification.save
    end
  end
end
