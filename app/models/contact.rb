class Contact < ActiveRecord::Base
  scope :phones, -> { where(contact_type: :phone) }

  scope :sites, -> { where(contact_type: :site) }
  scope :social_sites, -> { where(contact_type: :social_site) }
  scope :all_sites, -> { sites + social_sites }
end
