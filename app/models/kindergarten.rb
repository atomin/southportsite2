class Kindergarten < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def boss_post
    'Директор'
  end

  def self.icon
    'MapIcons/daycare.png'
  end

  def self.ru_name
    'Детские сады'
  end  

  def self.topic
    'Образование'
  end
end
