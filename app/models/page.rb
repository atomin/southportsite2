class Page < ActiveRecord::Base
  has_many :comments, as: :commentable
  include Notificable

  scope :work, -> { find_by name: 'work' }

  def url
    '/links'
  end

  def title
  	name
  end
end
