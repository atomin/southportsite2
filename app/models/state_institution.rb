class StateInstitution < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def self.icon
    'MapIcons/reception.png'
  end  

  def self.ru_name
    'Остальное'
  end  

  def self.topic
    'Гос. учреждения'
  end
end
