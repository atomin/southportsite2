class Cinema < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def self.icon
    'MapIcons/cinema.png'
  end  

  def self.ru_name
    'Кинотеатры'
  end  

  def self.topic
    'Культура'
  end
end
