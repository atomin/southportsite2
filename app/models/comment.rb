class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :commentable, polymorphic: true

  has_attached_file :picture, 
    styles: { medium: "400x400>"}, 
    default_url: "/images/:style/missing.png"

  validates_attachment_content_type :picture, 
    content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  def html_text
    text.gsub("\n", "<br/>").html_safe
  end

  def initialize(args=nil)
    super
    self.date = Time.now
    self.votes = 0
  end

  def add_notification(path, user)
    if commentable.respond_to? :add_notification
      commentable.add_notification(self, path, user)
    end
  end
end
