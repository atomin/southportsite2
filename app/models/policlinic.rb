class Policlinic < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def boss_post
    'Главный врач'
  end

  def self.icon
    'MapIcons/firstaid.png'
  end    

  def self.ru_name
    'Поликлиники'
  end  

  def self.topic
    'Медицина'
  end
end
