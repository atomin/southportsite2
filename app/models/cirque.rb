class Cirque < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def self.icon
    'MapIcons/festival.png'
  end

  def self.ru_name
    'Цирки'
  end  

  def self.topic
    'Культура'
  end
end
