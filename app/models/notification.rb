class Notification < ActiveRecord::Base

  belongs_to :ref_user, class_name: :User

  belongs_to :user
end
