class User < ActiveRecord::Base
  has_many :notifications

  def new_notifications
    notifications.where(read: false).order("created_at desc") || []
  end

  def old_notifications
    notifications.where(read: true).order("created_at desc") || []
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  devise :omniauthable

  validates :nickname, uniqueness: { case_sensitive: false }

  def login=(login)
    @login = login
  end

  def login
    @login || self.nickname || self.email
  end

  def self.from_omniauth(auth)
    pp auth
    where(auth.slice(:provider, :uid)).first_or_create do |user|
      nickname = auth.info.nickname.to_s.empty? ? auth.info.name : auth.info.nickname
      email = auth.info.email
      user.nickname = nickname.to_s.empty? ? email : nickname
      user.email = email
    end
  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def password_required?
    super && provider.blank?
  end

  def email_required?
    false
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(nickname) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end
end
