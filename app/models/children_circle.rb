class ChildrenCircle < ActiveRecord::Base
  has_many :contacts, as: :contactable
  has_many :comments, as: :commentable

  def boss_post
    'Директор'
  end

  def self.icon
    'MapIcons/kids.png'
  end

  def self.ru_name
    'Детские кружки'
  end

  def self.topic
    'Образование'
  end
end
