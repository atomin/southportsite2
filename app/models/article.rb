class Article < ActiveRecord::Base
  has_many :comments, as: :commentable

  belongs_to :author, class_name: :User

  scope :approved, -> { where(approved: true).order('id desc') }

  include Notificable
end
