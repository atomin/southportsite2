class InfoController < ApplicationController
  
  before_action :set_entity_class, only: [:index, :institution, :institution_page]

  def index
    @odds = []
    @evens = []

    @entityClass.order(:name).all.each_with_index do |x, i|
      ((i+1).odd? ? @odds : @evens) << x
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  def institution
    entity = @entityClass.find(params[:entityId])
    js = entity.as_json( include: [:contacts, :comments], methods: [:boss_post, :img_path] )
    if js['img_path']
      js['img_path'] = ActionController::Base.helpers.asset_path js['img_path'] 
    end
    render json: js
  end

  def institution_page
    @institution = @entityClass.find(params[:entityId])
  end

  def map
    klasses = [School, Stomatology, StateInstitution, Policlinic, Police, 
      MaternityHospital, Library, Kindergarten, Hospital, Cirque, Cinema, 
      ChildrenCircle, ControllerOffice]
    @entities = klasses.map do |k| 
      {
        name: k.to_s, 
        ruName: k.try(:ru_name) || k.to_s, 
        topic: k.try(:topic) || '', 
        all: k.all, 
        icon: ActionController::Base.helpers.asset_path(k.try(:icon) || 'MapIcons/alien.png')
      }
    end
  end

  private 

  def set_entity_class
    @entityName = params[:entityName]
    @entityName = 'School' if @entityName =~ /index/i
    @entityClass = Object.const_get(@entityName.singularize.camelize )
  end
end
