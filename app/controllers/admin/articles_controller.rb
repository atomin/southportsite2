class Admin::ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy, :set_approved]
  before_action :auth
  before_action :article_auth, only: [:edit, :update, :destroy]

  # GET /articles
  # GET /articles.json
  def index
    @articles = current_user.admin? ? Article : Article.where(author: current_user)
    @articles = @articles.order('date desc')
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)
    @article.author = current_user

    respond_to do |format|
      if @article.save
        format.html { redirect_to edit_admin_article_path(@article), notice: 'Article was successfully created.' }
        format.json { render action: 'show', status: :created, location: @article }
      else
        format.html { render action: 'new' }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to edit_admin_article_path(@article) , notice: 'Article was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def set_approved
    approved = params.require(:approved)
    respond_to do |format|
      if current_user.admin? && @article.update(approved: approved)
        format.json { head :no_content }
      else
        format.json { render json: { msg: 'error' }, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to admin_articles_path }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :header, :body, :date)
    end

    def auth
      unless current_user
        redirect_to root_url, alert: "Not authorized"
      end
    end

    def article_auth
      article = Article.find(params[:id])
      if !current_user.admin? && article.author != current_user
        redirect_to request.referrer, alert: "You have not enough rights for action"
      end
    end
end
