class NotificationsController < ApplicationController
  def index
    @old_notifications = current_user.old_notifications
    @new_notifications = current_user.new_notifications
  end
end
