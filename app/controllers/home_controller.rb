class HomeController < ApplicationController
  def index
    @articles = Article.approved.order('date desc').first(3)
  end
end
