class HelpfulController < ApplicationController
  def index
    @sections = YAML.load_file("#{Rails.root}/db/helpful.yml")
  end
end
