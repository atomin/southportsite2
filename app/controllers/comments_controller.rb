class CommentsController < ApplicationController
  before_action :set_comment, only: [:edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token, only: [:create]
  # GET /comments/1/edit
  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)

    respond_to do |format|
      if @comment.save
        path = owner_path(@comment, true)
        @comment.add_notification(path, current_user)
        format.html { redirect_to path }
        format.json { render action: 'show', status: :created, location: @comment }
        format.js
      else
        format.html { render action: 'new' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if params[:button] == 'cancel' || @comment.update(comment_params)
        format.html { redirect_to owner_path(@comment, true), notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
        format.js
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to owner_path(@comment) }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      p = params.require(:comment)
        .permit(:text, :commentable_id, :commentable_type, :picture)
      p[:user_id] = current_user.id
      p
    end

    def owner_path(comment, with_key = false)
      url = comment.commentable.try :url
      url ||= request.referrer
      url += "#comment_" + comment.id.to_s if with_key
      url
    end
end
