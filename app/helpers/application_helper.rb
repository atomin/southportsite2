module ApplicationHelper
  def comments(n)
    case
      when n == 0 then "Нет отзывов"
      when n % 10 == 1 && n % 100 != 11  then "#{n} отзыв"
      when (2..4) === n % 10 then "#{n} отзыва"
      else "#{n} отзывов"
    end
  end
end
