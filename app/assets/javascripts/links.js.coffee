onready ->
  submit = (e) ->
    code = e.keyCode || e.which
    if code in [10,13] && not e.ctrlKey
      e.preventDefault()
      $(this).submit()
      $(this).find('.comment_text_field').val('')
      return false

  $('#new_comment').on "keydown", submit 
  $('.comments-panel').on "keydown", '.edit_comment', submit


  $('.comment_text_field').on('keydown',  (e) ->
    code = e.keyCode || e.which
    if code in [10,13] && e.ctrlKey
      e.preventDefault()
      $(this).val($(this).val() + "\n")
      return false
  )

  $("#picture_button").on 'click', ->
    $("#hidden_picture_button").trigger('click')

  $('#hidden_picture_button').on 'change', (e) ->
    val = $('#hidden_picture_button').val()
    res = /(\\.+)*\\(.*)/.exec(val)
    $('#filename').text(res[2])

  $('.hidden_submit').hide()
  $('.hidden_cancel').hide()