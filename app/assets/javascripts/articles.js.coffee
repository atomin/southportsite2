onready ->
  $('.admin_articles input[value=approved]').change (me) -> 
    url = $(this).data('url')
    checked = $(this)[0].checked
    $.post url, approved: checked, _method: 'patch'

  l = window.location
  baseUrl = l.origin || l.protocol + "//" + l.host 
  tinymce.init
    selector: "[rel=tinymce]"
    theme: "modern"
    toolbar: "bold italic underline | bullist numlist outdent indent | undo redo | pastetext pasteword selectall | link uploadimage "
    pagebreak_separator: "<p class='page-separator'>&nbsp;</p>"
    plugins: ["uploadimage link preview code fullscreen"]
    relative_urls: false
    remove_script_host: false
    document_base_url: "#{baseUrl}/"
    style_formats: [
      {
        title: 'Image Left'
        selector: 'img'
        styles: 
          'float': 'left'
          'margin': '0 10px 0 10px'
      }, {
        title: 'Image Right'
        selector: 'img'
        styles: 
         'float': 'right'
         'margin': '0 0 10px 10px'
      }]