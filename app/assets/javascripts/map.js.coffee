window.initialize_map = -> 
  mapOptions = 
    center: 
      lat: 55.716583
      lng: 37.677400
    zoom: 14
    zoomControlOptions: 
      style: google.maps.ZoomControlStyle.SMALL
      position: google.maps.ControlPosition.TOP_RIGHT
    scrollwheel: false
    panControl: false
    mapTypeControlOptions:
      # style: google.maps.MapTypeControlStyle.INSET_LARGE
      position: google.maps.ControlPosition.BOTTOM_RIGHT
      # mapTypeIds: [google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE]
    
  areaCoordinates = [
    new google.maps.LatLng(55.705523, 37.686582),
    new google.maps.LatLng(55.704725, 37.690187),
    new google.maps.LatLng(55.697180, 37.684608),
    new google.maps.LatLng(55.697929, 37.680058),
    new google.maps.LatLng(55.698075, 37.676239),
    new google.maps.LatLng(55.698582, 37.674608),
    new google.maps.LatLng(55.700130, 37.672505),
    new google.maps.LatLng(55.700807, 37.669759),
    new google.maps.LatLng(55.701025, 37.667055),
    new google.maps.LatLng(55.705426, 37.667398),
    new google.maps.LatLng(55.718385, 37.663364),
    new google.maps.LatLng(55.725080, 37.663364),
    new google.maps.LatLng(55.726844, 37.662807),
    new google.maps.LatLng(55.730783, 37.660317),
    new google.maps.LatLng(55.731774, 37.664480),
    new google.maps.LatLng(55.730820, 37.665360),
    new google.maps.LatLng(55.730656, 37.666208),
    new google.maps.LatLng(55.730928, 37.667248),
    new google.maps.LatLng(55.725829, 37.670488),
    new google.maps.LatLng(55.727400, 37.677612),
    new google.maps.LatLng(55.727557, 37.678149),
    new google.maps.LatLng(55.728318, 37.678921),
    new google.maps.LatLng(55.720874, 37.700315),
    new google.maps.LatLng(55.714807, 37.679071)
  ]

  el = document.getElementById('map-canvas')
  window.map = new google.maps.Map(el, mapOptions)

  polyOptions =
    path: areaCoordinates
    strokeColor: "#FF0000"
    strokeOpacity: 1
    strokeWeight: 0.5
    fillColor: "#FF0000"
    fillOpacity: 0.05
    map: map
  it = new google.maps.Polygon(polyOptions)

  menu = $('#map-menu')
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(menu[0])

  source   = $("#institution-template").html()
  template = Handlebars.compile(source)
  
  window.markers = {}
  for en in entities
    for obj in en.all
      marker = new google.maps.Marker
        position: new google.maps.LatLng(obj.lat, obj.lng)
        map: map
        icon: en.icon
        title: obj.name
        entityName: en.name
        entityRuName: en.ruName
        entityId: obj.id

      markers[en.name] = [] unless markers[en.name] 
      markers[en.name].push marker
      google.maps.event.addListener(marker, 'click', getMarkerHandler(marker, template) )

  menu.show()

  $('input.entity-toggle').on('change', -> 
    setEntityVisibile(this)
    group = $(this).closest('.entities_group')
    toggles = group.find('input.entity-toggle')
    for t in toggles when t.checked
      return

    #все выключены значит можно выключить группу
    groupToggle = group.find('input.group-toggle')
    groupToggle[0].checked = false
    toggleGroup(group)
    
  )

  $('input.group-toggle').on('change', -> 
    group = $(this).closest('.entities_group')
    toggles = group.find('input.entity-toggle')
    for t in toggles
      t.checked = this.checked
      setEntityVisibile(t)

    toggleGroup(group)
  )

  $('#collapse-button').on('click', ->
    $('.menu-inner').slideToggle()
    $('.collapse-icon').toggle()
    $('.expand-icon').toggle()
    collapsed = $('.expand-icon').is(':visible')
    $('#collapse-button').height(if collapsed then 31 else 16)
  )

setEntityVisibile = (toggle) ->
  entity = $(toggle).data('entity')
  m.setVisible(toggle.checked) for m in markers[entity]

toggleGroup = (group) ->
  menu = group.find('ul.menu')
  menu.toggle()
  visible = menu.is(':visible')
  group.css('color', if visible then '#000' else '#888')


getMarkerHandler = (marker, template)-> 
  m = marker
  return ->
    $.get('institution', { entityName: m.entityName, entityId: m.entityId }, (resp)->
      window.curWindow.close() if window.curWindow

      resp.phones = (c for c in resp.contacts when c.contact_type == 'phone')
      resp.sites = (c for c in resp.contacts when c.contact_type in ['site', 'social_site'])
      resp.entityName = m.entityName

      n = resp.comments.length
      resp.commentsCountString = switch
        when n == 0 then "Нет отзывов"
        when n % 10 == 1 && n % 100 != 11  then "#{n} отзыв"
        when n % 10 in [2..4] then "#{n} отзыва"
        else "#{n} отзывов"

      window.curWindow = new google.maps.InfoWindow
        content: template(resp)
      window.curWindow.open(map, m)
    )