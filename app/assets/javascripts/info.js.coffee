onready ->
  menuPositionInit()
  menuToggleInit()

menuPositionInit = ->  
  $(document).scroll ->
    y = $(document).scrollTop()
    header = $(".info-menu")

    if y >= 150
      header.addClass('fixed')
    else 
      header.removeClass('fixed')

menuToggleInit = ->
  $("li.main").click ->
    count = $(this).data('count')
    $(this).nextAll('.sub-menu-item').slice(0, count).slideToggle()

